# fibersetup

# Fancy setup

```mermaid

flowchart LR
subgraph freedom internet
    glasvezel
end

subgraph CCR2004-1G-12S+2XS
    glasvezel<-.->ONTSFPPLUS
    SFPPLUS1(woonkamerglas)
    SFPPLUS2(micahglas)
    SFPPLUS3(mateoglas)
    SFPPLUS4(kantoorglas)
    SFPPLUSRJ45(poeswitch)
end

SFPPLUS1 <-.-> fiber2rj45_woonkamer
SFPPLUS2 <-.-> fiber2rj45_micah
SFPPLUS3 <-.-> fiber2rj45_kantoor
SFPPLUS4 <-.-> fiber2rj45_mateo
SFPPLUSRJ45 --> PORT1

subgraph CRS112-8P-4S-IN
    PORT1
    PORT2
    PORT3
    PORT4
    PORT5
    PORT6
    PORT7
    PORT8
end

PORT2 --> hap_xl_ax
PORT3 --> hap_ax3_micah
PORT4 --> hap_ax3_kantoor
PORT5 --> hap_ax3_mateo


subgraph woonkamer
    fiber2rj45_woonkamer(woonkamer/FTC11XG)
    hap_xl_ax(Wireless AP)
end

subgraph micah
    hap_ax3_micah(Wireless AP)
    fiber2rj45_micah(micah/FTC11XG)
end

subgraph kantoor
    hap_ax3_kantoor(Wireless AP)
    fiber2rj45_kantoor(kantoor/FTC11XG)
end

subgraph mateo
    hap_ax3_mateo(Wireless AP)
    fiber2rj45_mateo(mateo/FTC11XG)
end

```


# Goedkopere optie met max 1Gbps

```mermaid

flowchart LR
subgraph freedom internet
    glasvezel(fa:fa-globe Glasvezel)
    zyxel
    glasvezel --> zyxel
end
zyxel<-->RPORT1

subgraph RB5009UG+S+
    RPORT1(fa:fa-ethernet PORT1)
    RPORT2(fa:fa-ethernet PORT2)
end

RPORT2 --> PORT8

subgraph CRS112-8P-4S-IN
    SFPPLUS1(micahglas)
    SFPPLUS2(mateoglas)
    SFPPLUS3(kantoorglas)
    PORT1(fa:fa-ethernet PORT1)
    PORT2(fa:fa-ethernet PORT2)
    PORT3(fa:fa-ethernet PORT3)
    PORT4(fa:fa-ethernet PORT4)
    PORT5(fa:fa-ethernet PORT5)
    PORT6(fa:fa-ethernet PORT6)
    PORT7(fa:fa-ethernet PORT7)
    PORT8(fa:fa-ethernet PORT8)
end

SFPPLUS1 <-.-> fiber2rj45_micah
SFPPLUS2 <-.-> fiber2rj45_kantoor
SFPPLUS3 <-.-> fiber2rj45_mateo

PORT1 --> hap_xl_ax
PORT2 --> hap_ax3_micah
PORT3 --> hap_ax3_kantoor
PORT4 --> hap_ax3_mateo
PORT5 --> utp_muur_tv


subgraph woonkamer
    hap_xl_ax(fa:fa-wifi Wireless AP plafond)
    utp_muur_tv
end

subgraph micah
    hap_ax3_micah(fa:fa-wifi Wireless AP)
    fiber2rj45_micah(fa:fa-circle-nodes micah/FTC11XG)
end

subgraph kantoor
    hap_ax3_kantoor(fa:fa-wifi Wireless AP)
    fiber2rj45_kantoor(fa:fa-circle-nodes kantoor/FTC11XG)
end

subgraph mateo
    hap_ax3_mateo(fa:fa-wifi Wireless AP)
    fiber2rj45_mateo(fa:fa-circle-nodes mateo/FTC11XG)
end

```